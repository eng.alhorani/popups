<?php

namespace App\Jobs;

use App\Models\Popup;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SavePopupJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    private $image=null;
    private $popup=null;
    public function __construct($popup,$image)
    {
        $this->popup=$popup;
        $this->image=$image;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $popup = Popup::create([
            'title' => $this->popup['title'],
            'content' => $this->popup['content'] ,
            'type' => $this->popup['type'],
            'user_id' => $this->popup['user_id'],
            'cover_image'=>$this->image,
        ]);

        $popup->save();
    }
}
