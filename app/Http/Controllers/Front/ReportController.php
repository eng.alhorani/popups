<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;
use App\Models\Popup;
use DataTables;
class ReportController extends Controller
{

    public function type():JsonResponse
    {
        $popups = DB::table('popups')
            ->select(DB::raw('count(title) as numberPopup,  type'))
            ->groupBy('type')
            ->orderBy('type')
            ->get();
        $type =[];$i=0;
        $popupNumber =[];
        foreach ($popups as $data)
        {
            array_push($type,$popups[$i]->type);
            array_push($popupNumber,$popups[$i]->numberPopup);
            if (sizeof($popups) - 1 > $i)
                $i++;
        }


        return response()->json(['type'=>$type,'popupNumber'=>$popupNumber]);
    }
    public function table():JsonResponse
    {
        $data = DB::table('popups')
            ->select(DB::raw('count(title) as numberPopup,  type'))
            ->groupBy('type')
            ->orderBy('type')
            ->get();
            // return response()->json($datas);
            return response()->json(['data' => $data]);
    }

}
