<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\PopupRequest;
use App\Jobs\SavePopupJob;
use App\Models\Popup;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Repositories\PopupRepository;
class PopupController extends Controller
{
    private  $popupRepository;
    public function __construct(PopupRepository $popupRepository)
    {
        $this->popupRepository = $popupRepository;
    }
    public function index()
    {
        $popups = Popup::orderBy('created_at','desc')->paginate(9);
        return view('home', compact('popups'));
    }
    public function showPopup(Popup $popup)
    {
        return view('showPopup', compact('popup'));
    }
//dashboard
    public function dashboard(Request $request)
    {
        $user = User::query()->findOrFail(auth()->id());
        $popups = Popup::query()->whereUserId($user->id)->paginate(10);
        return view('dashboard',compact('popups'));
    }
//delete
    public function destroy($id, Request $request)
    {
        $popup = Popup::query()->find($id);

        $this->popupRepository->delete($popup);

      $request->session()->flash('success','popup deleted successfully');

        return redirect(route('dashboard'));
    }
// store
    public function store(PopupRequest $request)
    {
        $popup['title'] = $request->title;
        $popup['content'] = $request->content;
        $popup['type'] = $request->type;
        $popup['user_id'] = $request->user_id;
       $cover_image = uploadFile('cover_image', 'popup');
       //using job queu
        SavePopupJob::dispatch($popup,$cover_image);
        return redirect(route('dashboard'));
    }
//edit
    public function edit(Popup $popup)
    {
     return   view('create',compact('popup'));
    }
    public function update(PopupRequest $request,$id)
    {
        $popup = Popup::query()->find($id);
        $this->popupRepository->update($request, $popup);

        return   redirect(route('dashboard'));
    }

}
