<?php

namespace App\Http\Controllers\Api\Popup;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Popup;
use Illuminate\Http\Request;
use App\Http\Requests\PopupRequest;

use App\Repositories\PopupRepository;
use Illuminate\Http\JsonResponse;

class PopupController extends  ApiController
{
    private $popupRepository;

    public function __construct(PopupRepository $popupRepository){
        $this->popupRepository = $popupRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): JsonResponse
    {
        $limit = $request->get('limit') ? : 10 ;
        if($limit > 30 ) $limit =30 ;
        $request->request->add(['status' => 1]);

        $popups = $this->popupRepository
            ->getPopups($request)
            ->paginate($limit);
        $popupsPage= $popups->all();
        return $this->respondSuccess($popupsPage, $this->createApiPaginator($popups));

    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(PopupRequest $request): JsonResponse
    {

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondError(__('api.user_not_found'));
        $request->request->add(['user_id' => $user->id]);
        $this->popupRepository->add($request);

        return $this->respondSuccess();
    }



    public function popup($id, Request $request): JsonResponse
    {

        $popup = Popup::query()
            ->where('id', $id)
            ->first();

        if (!$popup)
            return $this->respondError(__('api.note_not_found'));


        return $this->respondSuccess($popup);
    }





    public function myPopups(Request $request): JsonResponse
    {
        $limit = $request->get('limit') ? : 10 ;
        if($limit > 30 ) $limit =30 ;

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondSuccess(__('api.user_not_found'));

        $popups = Popup::query()->whereUserId($user->id)->paginate($limit);


        return $this->respondSuccess($popups->all(), $this->createApiPaginator($popups));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, PopupRequest $request): JsonResponse
    {
        $popup = Popup::query()->find($id);
        if (!$popup)
            return $this->respondError(__('api.item_not_found'));

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondError(__('api.user_not_found'));

        if ($user->id != $popup->user->id)
            return $this->respondError(__('api.unauthorized'));

        $this->popupRepository->update($request, $popup);

        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request): JsonResponse
    {
        $popup = Popup::query()->find($id);
        if (!$popup)
            return $this->respondError(__('api.item_not_found'));

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondError(__('api.user_not_found'));

        if ($user->id != $popup->user->id)
            return $this->respondError(__('api.unauthorized'));

        $this->popupRepository->delete($popup);

        return $this->respondSuccess();
    }


}
