<?php

namespace App\Repositories;

use App\Models\Popup;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;
use function auth;
//use function convertToSeparatedTokens;

class PopupRepository {

        public function add(Request $request)
        {
            $popup = Popup::create([
                'title' => $request->title,
                'content' => $request->content ,
                'type' => $request->type,
                'user_id' => $request->user_id,
            ]);

            $popup->cover_image = uploadFile('cover_image', 'popup');

            $popup->save();

        }

    public function getPopups(Request $request)
    {
        $popups = Popup::query();

        if ($search = $request->get('search')) {
            $tokens = convertToSeparatedTokens($search);
            $popups->whereRaw("MATCH(title, content) AGAINST(? IN BOOLEAN MODE)", $tokens);
        }
        return $popups;
    }


    public function update(Request $request, Popup $popup)
    {
        $popup->update($request->all());
        if ($request->hasFile('cover_image')) {
            // if there is an old background_image delete it
            if ($popup->cover_image != null) {
                $popup->cover_image = Storage::disk('public')->delete($popup->cover_image);
            }
            $popup->cover_image = uploadFile('cover_image', 'popup');
        }
    $popup->save();
        return $popup;
    }


    public function delete(Popup $popup)
    {
//        if ($popup->cover_image != null)
//            $popup->cover_image = Storage::disk('public')->delete($popup->cover_image);

        $popup->delete();
    }

}
