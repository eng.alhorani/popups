<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>Cms</title>
  @include('includes._header')
</head>
    <body class="antialiased">
    @include('includes._nav')
    <div class="container bg-gray-100" >
        <div class="w-100 my-4">
            @if (session('status'))
                <div class="alert alert-success" role="alert">
                    {{ session('status') }}
                </div>
            @endif
            <h2 class="color-black">Popups</h2>

        </div>
        <div class="d-flex flex-wrap justify-content-around">
            @foreach($popups as $popup)
                <div class="card my-2 mx-2 " style="width: 18rem">
                    {{-- <img src="{{storageImage($popup->cover_image)}}" class="card-img-top" alt="..." > --}}
           <img src="{{ storageImage($popup->cover_image) ? storageImage($popup->cover_image) : asset('assets/img/no-image.png') }}" class="card-img-top" style="height: 200px">

                    <div class="card-body">
                        <h5 class="card-title">{{$popup->title}}</h5>
                        <p class="card-text">   {!!   Illuminate\Support\Str::limit( strip_tags($popup->content), 50) !!}...
                        </p>
                        <a href="{{route('showPopup',$popup)}}" class="btn btn-primary">show</a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="w-100 py-2">
            {{ $popups->links() }}
        </div>

    </div>

       @include('includes._scripts')
    </body>
</html>
