<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <a href="{{route('index')}}"> <title>PopUps</title> </a>
    @include('includes._header')
</head>
<body class="antialiased">
@include('includes._nav')

<div class="container">
    <a href="{{route('create')}}" type="button" class="btn btn-primary my-2">Add</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">image</th>
            <th scope="col">Title</th>
            <th scope="col">content</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
       @foreach($popups as $popup)
        <tr>
            <th scope="row">{{$popup->id}}</th>
            <td>
{{--            <img src="{{storageImage($popup->cover_image)}}" class="" style="width:75px ; height: 75px">--}}
                {{-- <img onerror="this.src='{{storageImage($popup->cover_image)}}';"
                     src="{{asset('assets/img/no-image.png')}}"> --}}
           <img src="{{ storageImage($popup->cover_image) ? storageImage($popup->cover_image) : asset('assets/img/no-image.png') }}" style="width:75px ; height: 75px">

            </td>
            <td>{{$popup->title}}</td>
            <td>{{$popup->content}}</td>
            <td>
                <div class="py-2">
                    <form class="float-right ml-2"
                          action="{{route('destroy', $popup->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger btn-sm my-1">
                            {{--                        {{ $popup->trashed() ? 'Delete' : 'Trash' }}--}}
                            Delete
                        </button>
                    </form>
                    <a class="mx-2 my-1 btn btn-primary float-right btn-sm" href="{{route('edit',$popup)}}">
                        Edit
                    </a>

                </div>


            </td>
        </tr>
       @endforeach


        </tbody>
    </table>


    {{ $popups->links() }}

</div>

@include('includes._scripts')
</body>
</html>
