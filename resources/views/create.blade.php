<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>create popup</title>
    @include('includes._header')
</head>
<body class="antialiased">
@include('includes._nav')

<div class="container">


    <div class="card card-default">
        <div class="card-header">
         {{isset($popup) ? "update popup":"add new popup"}}
        </div>
        <div class="card-body">
            <form action="{{isset($popup) ? route('update',$popup->id) : route('store') }}" method="POST" id="formId" enctype="multipart/form-data">
                @csrf
                @if (isset($popup))
                    @method('PUT')
                @endif
                <div class="form-group">
                    <label for="popup title">Title:</label>
                    <input type="text" class="form-control" name="title" placeholder="Add a new popup" value="{{ isset($popup) ? $popup->title : "" }}">
                </div>
                <div class="form-group">
                    <label for="popup content">Content:</label>
                    <textarea class="form-control @error('content') is-invalid @enderror " rows="2" name="content" placeholder="Add a content">{{ isset($popup) ? $popup->content : "" }}</textarea>
                    @error('content')
                    <div class="invalid-feedback " role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>
        <div>
            <label for="popup image">Type:</label>
            <select name="type" class="custom-select custom-select-lg mb-3">
                <option selected>{{ isset($popup)? $popup->type :'Open this select menu'}}</option>
                <option value="full-screen">full-screen</option>
                <option value="slide-in">slide-in</option>
                <option value="exit-intent">exit-intent</option>
            </select>
        </div>


                <div class="form-group">
                    <label for="popup image">Image:</label>
                    <input type="file" class="form-control @error('cover_image') is-invalid @enderror" name="cover_image">
                    @error('cover_image')
                    <div class="invalid-feedback " role="alert">
                        {{ $message }}
                    </div>
                    @enderror
                </div>

                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                <div class="form-group">
                    <button type="submit" class="btn btn-success">
                      {{ isset($popup) ? "Update" : "Add" }}
                    </button>
                </div>
            </form>
        </div>
    </div>



</div>
@include('includes._scripts')
<script>

</script>

</body>
</html>
