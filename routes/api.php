<?php

use App\Http\Controllers\Api\Popup\PopupController;
use App\Http\Controllers\Api\User\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login', [UserController::class, 'login']);
Route::post('register', [UserController::class, 'register']);
//Route::post('logout', [UserController::class, 'logout']);
Route::group(['middleware' => 'auth:sanctum'], function(){
    Route::post('logout',[UserController::class,'logout']);

    Route::post('new-note', [PopupController::class, 'store']);
    Route::post('/update/{id}', [PopupController::class, 'update']);
    Route::delete('/delete/{note}', [PopupController::class, 'destroy']);

    Route::get('my-notes', [PopupController::class, 'myNotes']);

});

Route::get('notes', [PopupController::class, 'index']);
Route::get('note/{id}', [PopupController::class, 'note']);

