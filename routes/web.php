<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\PopupController;
use App\Http\Controllers\Front\ReportController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', [PopupController::class, 'index'])->name('index');
Route::get('/popup/{popup}', [PopupController::class, 'showPopup'])->name('showPopup');
Route::GET('/type',[ReportController::class, 'type'])
->name('type');
Route::get('/reports',function () {
    return view('reports');
})->name('reports');
Route::get('/table', [ReportController::class, 'table'])->name('table');

Route::group(['middleware'=>'auth'],function (){
    Route::get('/dashboard', [PopupController::class, 'dashboard'])->name('dashboard');
    Route::delete('/destroy/{id}', [PopupController::class, 'destroy'])->name('destroy');
    Route::post('/store', [PopupController::class, 'store'])->name('store');
    Route::get('/create',function () {
        return view('create');
    })->name('create');

    Route::get('/edit/{popup}',[PopupController::class,'edit'])->name('edit');
    Route::put('/update/{id}',[PopupController::class,'update'])->name('update');

});
require __DIR__.'/auth.php';
